addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request));
})
/**
 * Respond with hello worker text
 * @param {Request} request
 */

async function fetch_variant(variant_num, variant_urls) {
 	const init = {
     	headers: {
       		'content-type': 'text/html;charset=UTF-8',
     	},
   	}
 	
 	const selected_variant_url = variant_urls["variants"][variant_num]

 	let response = await fetch(selected_variant_url, init)

 	// reconstruct response to make headers mutable
	response = new Response(response.body, response)
	response.headers.set('Set-Cookie', `variant_${variant_num}`)

	// rewrite to custom HTML 
	const rewriter = new HTMLRewriter();
	rewriter.on('h1#title', new CustomRewriter(variant_num));
	rewriter.on('p#description', new CustomRewriter(variant_num));
	rewriter.on('a#url', new CustomRewriter(variant_num));
	rewriter.on('div.pb-6', new CustomRewriter(variant_num))

 	return rewriter.transform(response);
}	
async function handleRequest(request) {

  	// request URLs from API
	const variant_response = await fetch("https://cfw-takehome.developers.workers.dev/api/variants")
	const variant_urls = await variant_response.json()

	// Distribute requests between variants, but persist variants with cookies
	const cookie = request.headers.get("cookie")

	// if cookies present, show user the same variant
	if (cookie && cookie.includes("variant_0")) {
		return fetch_variant(0, variant_urls)
	} else if (cookie && cookie.includes("variant_1")) {
		return fetch_variant(1, variant_urls)
	} else {
		// select variant url randomly with 50/50 dist if no cookies
		const ZERO_OR_ONE = Math.round(Math.random())

		return fetch_variant(ZERO_OR_ONE, variant_urls)

	}
}

class CustomRewriter {
  constructor(variant_num) {
    this.variant_num = variant_num
  }

  element(element) {
    const attribute = element.getAttribute("href")
    if (attribute) {
    	switch (this.variant_num) {
    		case 0:
    			element.setAttribute(
    			  "href",
    			  attribute.replace("https://cloudflare.com", 
    			  	"https://www.linkedin.com/in/peter-pao-huang-22007114a/"),
    			)
    			break;
			case 1:
				element.setAttribute(
    			  "href",
    			  attribute.replace("https://cloudflare.com", "https://epius.ai/"),
    			)
    			break;
    	}
      
    }
    if (element.tagName == "div") {
    	switch (this.variant_num) {
    		case 0:
    			element.setAttribute("style", 
    				"background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRCsO7d3Sbc07T0CXS3NEHt2VfvJd7fz4z7XdEeKfWpv47EGw1m&usqp=CAU')")
    			break;
			case 1:
				element.setAttribute("style", 
    				"background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRo47-tj-dU4Jsh484gIFGHzYjYXJ_A5Z2xhaAOR_V1tb_UK8MO&usqp=CAU')")
    			break;
		}
    }
  }
  text(text){
  	switch (this.variant_num) {
  		case 0:
  			if (text.text.includes("Variant ")) {
  				text.replace("Hi! I'm Peter Pao-Huang")
  			}
  			if (text.text.includes("of the take home project!")) {
  				text.replace("Creative, passionate, and eager to learn")
  			}
  			if (text.text.includes("Return to cloudflare.com")) {
  				text.replace("Check out my LinkedIn")
  			} 
			break;
		case 1:
			if (text.text.includes("Variant ")) {
  				text.replace("Pleasure to meet you, I'm Peter")
  			}
  			if (text.text.includes("of the take home project!")) {
  				text.replace("I believe I'd be a great addition to your team!")
  			}
  			if (text.text.includes("Return to cloudflare.com")) {
  				text.replace("Check out my most recent project")
  			} 
			break;
  	}
  	
  }
}
